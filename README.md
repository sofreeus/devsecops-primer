# DevSecOps

**This class will:**

- Walk an application through a simple development life-cycle (prepare, build, test, deploy)
- Demonstrate steps to integrate security to a CI/CD pipeline, without disrupting the SDLC

---

### Success Metric

Measure - Successfully reduce the number of vulnerabilities in a web application without rewriting any code.

---

### Prerequisites:

- Before starting this class, you should have a basic familiarity with a Unix-like shell (sh, bash, zsh, ksh, etc.)
- Laptop
  - A web browser - Chrome (or a Chrome-based browser like Brave) is probably best for this class
- [Create a Google Cloud account](https://cloud.google.com/)
  - This should ideally be a personal account (unless you have specific permission to spend money in your company's account)
  - New users should be eligible for [$300 starter credit](https://cloud.google.com/free/)
- [Create a GitLab account](https://gitlab.com/users/sign_in)
  - If you already have one, you can disregard this
  - If you create a new one, you might consider making it something that you could take from company to company

***

### Following Instructions

Please read and follow all directions closely. There are some complex nuances. Missing one might cause unexpected problems later.

After completing the steps verbatim the first time, by all means try variations later as this will help reinforce learning. Especially with things like paranoia levels, etc... There are also more nuanced ways to tune the web app firewall with config files that I don't touch on.

For more info, I recommend these links:

https://owasp.org/www-project-zap/ [zap]

https://owasp.org/www-project-modsecurity-core-rule-set/ [waf]

https://owasp.org/www-project-juice-shop/ [MyWebserver]

***



[Basic steps in any CI/CD pipeline]:

![image](basic-gitlab-ci.png?)

---

[SW Developer's View]:

![image](devops-activities.png?)

---

[Security View]:

![image](devsecops-activities.png?)

---

# Labs

### _Fundamentals_

### 0. [Set up and containerized WAF](labs/00_containerized_waf/README.md)

### 1. [GitLab-CI](labs/01_gitlab-ci/README.md)

---

### other links and thanks:

- Thanks to Mohammed A. Imran - I don't know him but he made this [slide deck](https://www.owasp.org/images/0/0f/Devsecops-owasp-indonesia.pdf) which I found to be helpful and I used a few screenshots from it

- Special thanks to **Alex Wise** for recommending the OWASP tools, and **Aaron Brown** for developing [Intro To DevOps](https://gitlab.com/sofreeus/introtodevops)
