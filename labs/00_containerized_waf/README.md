# Google Cloud Shell and GitLab Prep Work

Open your [Google Console](https://console.cloud.google.com/), and click to open your Cloud Shell:

![image](google-cloud-shell.png?)

---

### Get Started (Authentication)

- Set up [SSH keys](https://docs.gitlab.com/ce/gitlab-basics/create-your-ssh-keys.html)
  - Check to see if you have a public/private key pair in `~/.ssh/`
    - If so, you should see `id_rsa` and `id_rsa.pub` in that directory
    - If not, run `ssh-keygen` to generate a key pair
  - Log in to [GitLab](https://www.gitlab.com), navigate to the `SSH Keys` tab of your profile settings, and add the contents of your `id_rsa.pub` file as a new key.
- Add a [personal access token](https://gitlab.com/profile/personal_access_tokens) with API Scope to your GitLab account settings. Save the token value somewhere.


Make a directory called sfs and cd into it

```
mkdir sfs
cd ~/sfs
```

Now make an API auth token variable called "TOKEN", enter the token value into it

- Use the GitLab API to create a new repository

  ```bash
  TOKEN="<your_personal_access_token>"
  curl -X POST -H "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects?name=MyWebserver" | tee results
  ```

  ```bash
  cat results | jq
  # or
  cat results | jq '.ssh_url_to_repo'
  ```
- Take note of the `ssh_url_to_repo` field's value in the output of that command

- Clone your `mywebserver` repository locally

  ```bash
  git clone <value_of_ssh_url_to_repo>

  ```
---

### Clone The Class Materials

```bash
cd ~/sfs
git clone https://gitlab.com/sofreeus/devsecops-primer.git
```
---

#################################################################


# A WebServer, WAF, and ZAP (in Docker)

 * build and run a local webserver container (juice shop), put a waf in front of it

```bash
cd ~/sfs/mywebserver
```

copy some files from the class material into your working directory (we'll use them a bit later in this lab):

```bash
cp ~/sfs/devsecops-primer/labs/00_containerized_waf/files/customize.yml .
cp ~/sfs/devsecops-primer/labs/00_containerized_waf/files/generated_file.conf .
cp ~/sfs/devsecops-primer/labs/00_containerized_waf/files/Dockerfile .
```

verify they copied over:

```bash
ls
```

---

### Make a Webserver with Docker

Review the `Dockerfile` file and the `customize.yml` file:

```
cat Dockerfile
```

```yml
FROM bkimminich/juice-shop
COPY customize.yml ./config

```

```
cat customize.yml
```

```yaml
name: 'Software Freedom School OWASP Juice Box'

welcomeBanner:
  showOnFirstStart: True
  title: 'Welcome SFSers! Nothing Better To Do On A Saturday?'
```

And build your Docker image:

```bash
docker build -t webserver .
```
---

### Run and Test Your Webserver

Run your webserver with the `-p 8080:3000` option to expose the ports, and give it a name with the `--name` flag:

```bash
docker run --detach -e "NODE_ENV=customize" -p 8080:3000 --name mywebserver webserver
```

Now run `docker ps` to see information about your running container:

```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
6e1b4f9995ab        webserver           "docker-entrypoint.s…"   12 minutes ago      Up 12 minutes       0.0.0.0:8080->3000/tcp   mywebserver
```

Finally, test your webserver to see if it's running, use web-preview, change port to 8080 if needed:

![image](gcs-web-preview-change-port.png?)


if not using google cloud shell:
```bash
xdg-open http://localhost:8080

```

---

### WAF and OWASP ZAP

*first inspect your webserver container for the Docker Gateway IP address, store IP address in a variable called DGIP:*
```bash
DGIP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.Gateway}}{{end}}' mywebserver)
```
*verify the Docker Gateway IP address (DGIP) was captured in the variable:*

```bash
echo $DGIP
```

*attack your webserver directly (no WAF set up yet, attack port 8000), generate a report of the results:*

![image](i2dso-direct-attack.png?)

```bash
docker run -v $(pwd):/zap/wrk/:rw --name zap -t owasp/zap2docker-weekly:latest zap-baseline.py -t "http://$DGIP:8080" -c generated_file.conf -r shiny_report_no_waf_MANUAL.html
```

*review the vulnerability report. From the cloud shell, click 'Open Editor'*


![image](gcp-open-editor.png?)

*in the file explorer window that pops up on the left side locate your 'mywebserver' folder and click the file 'shiny_report_no_waf_MANUAL.html', then click the 'preview' button on the right:*

![image](gcp-report-preview.png?)

*the rendered version of the report file should now be visible and look something like this:*

![image](gcp-report-rendering-alt.png?)

*clearly a lot of security gaps, we'll fix that*

*toggle back to the terminal*

*now put a WAF in front of your webserver (set PARANOIA level to 2):*

![image](container_setup.png?)

```bash
docker run -d -p8081:80 --name waf -e PROXY=1 -e BACKEND=http://$DGIP:8080/ -e PARANOIA=2 owasp/modsecurity-crs:v3.3.2-apache
```

*verify the WAF is passing traffic to your webserver*

*do this by going back to the web-preview button in Google Cloud Shell, change the port to 8081*

![image](gcs-web-preview-change-port.png?)

*or just do it in the command line...*


```bash
curl http://$DGIP:8081
```


*(the **waf** is listening on **port 8081**, so if you can get to your website through it, then the **waf is successfully passing traffic**)*

*remove the ZAP docker image you ran before:*

```bash

docker rm -f zap
```


*NOW, run ZAP again but this time, attack your webserver INDIRECTLY (through the **WAF, port 8081**), change report name:*
```bash
docker run -v $(pwd):/zap/wrk/:rw --name zap -t owasp/zap2docker-weekly:latest zap-baseline.py -t "http://$DGIP:8081/" -c generated_file.conf -r shiny_report_waf_p2_MANUAL.html
```
*review the vulnerability report same way as before*
*HINT: Cloud Shell - Open Editor --> File Explorer on the left (click the file 'shiny_report_waf_p2_MANUAL.html') --> Preview button on right*

![image](gcp-report-rendering2-alt.png?)

*number of successful attacks are drastically reduced*

*the containerized web app firewall is doing its job!*

*next we'll transfer all this to a CI pipeline*

---

| Next: [GITLAB-CI](/labs/01_gitlab-ci/README.md)|
|:----:|
