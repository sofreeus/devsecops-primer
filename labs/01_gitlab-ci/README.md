# Continuous Integration

*By the end of this lab, you will:*
1. Understand authentication from your laptop to gitlab
1. Configure a gitlab pipeline
1. Setup a stage to build, test and push the webserver
1. Define Continuous Integration
1. Explain what _pipeline_, _stage_, _jobs_ and _scripts_ mean in the context of Continuous Integration

Required working directory: `~/sfs/mywebserver`  
(This is the root of your webserver repository)

---

Most continuous integration platforms start by cloning your repository.  You're then able to run build/test jobs against a clean environment with the latest copy of your (or your developer's) code.  You can accomplish something very similar by creating a new directory on your machine and cloning a new copy of your repo - which can be a really good way to test quickly and troubleshoot errors with your CI system.

---


GitLab has [Pipelines](https://docs.gitlab.com/ce/ci/pipelines.html) that perform continuous integration tasks for us.  

To use pipelines, we just need to create a `.gitlab-ci.yml` file.  There are [a lot of options](https://docs.gitlab.com/ce/ci/yaml/README.html) for this file, but we're just going to cover some basics.

---

### Terminology

*pipeline* - A collection of stages, jobs, and scripts that will be run when a new commit is checked in to the repository.
*stages* - Will be run sequentially.  
*jobs* - Will be run in parallel, within the confines of each stage.  
*scripts* - Will be run sequentially within the confines of each job.  

---

### A Basic GitLab CI Pipeline

Save the following as `.gitlab-ci.yml` in your webserver project at `~/sfs/mywebserver/.gitlab-ci.yml`

```yaml
image: docker:stable
services:
  - name: docker:dind

build_owasp-juice_shop_and_curl:
  stage: build

  script:
    - docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} -t ${CI_REGISTRY_IMAGE}:latest .
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
    - docker push ${CI_REGISTRY_IMAGE}
    - docker run --detach -e "NODE_ENV=customize" -p 8080:3000 --name mywebserver ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
    - docker logs mywebserver
    - DGIP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.Gateway}}{{end}}' mywebserver)
    - sleep 15
    - docker logs mywebserver
    - docker run --name curl curlimages/curl http://$DGIP:8080 # <---- fancy Docker way of doing a curl test
    - docker push ${CI_REGISTRY_IMAGE}
    - echo ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} published.

owasp_zap_test:
  stage: test
  image: docker:stable
  services:
    - name: docker:dind

  script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
    - docker run --detach -e "NODE_ENV=customize" -p 8080:3000 --name mywebserver ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
    - sleep 15
    - DGIP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.Gateway}}{{end}}' mywebserver)
    - echo $DGIP
    - docker run -d -p8081:80 --name waf -e PROXY=1 -e BACKEND=http://$DGIP:8080/ -e PARANOIA=2 owasp/modsecurity-crs:v3.3.2-apache
    - docker run -v $(pwd):/zap/wrk/:rw --name zap -t owasp/zap2docker-weekly:latest zap-baseline.py -t "http://$DGIP:8081" -c generated_file.conf -r shiny_report_CI.html
    - docker logs zap

  artifacts: # <--------------- place a copy of the ZAP attack report in the job status page of the repo
    when: always
    paths:
      - ./*.html
    expire_in: 1 day

```

- The `docker logs mywebserver` line above is a very helpful command that will proactively add troubleshooting information to the logs.

- Curl command (really a separate container whose one job is to execute curl) is included so we can see the changes we made to
the webserver in the output whose one job is to execute curl of Job Status Page (more on that in a second).

---

### Kicking it Off

- After you save your `.gitlab-ci.yml` file, you'll want to commit your changes to GitLab:

  ```bash
  cd ~/sfs/mywebserver
  git status
  # *after* verifying that all new and changed files are desired
  # and associated with a single change
  # add everything to the stage
  git add -A
  # and commit with a decription the fills in the blank
  # "apply this commit to ___"
  git commit -m 'add GitLab CI configuration'
  git push
  ```

---

### Checking Progress

- Go to [GitLab](https://gitlab.com) and navigate to your project
  - Should be: `https://gitlab.com/<your_user_name>/mywebserver`
- Click the `Pipelines` link
- If you don't see anything here, make sure you saved your `.gitlab-ci.yml` in the correct location, added, committed, and pushed your repo.
- You should see a `pending` or `running` (or maybe `passed` or `failed`) pipeline
- On the main Pipelines page, click on the status icon to see the pipeline details
- On the details page, click on the status icon to get information about the build (job) status
- You should see the output from your job on this page
  - This is where you can see the `docker logs ...` output
  - Take note of the output of the `${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}` command.
- Under `Packages` in the left nav menu, click `Container Registry` to view your image in the GitLab registry

---

## Checkpoint

**This is a critical point.  If things don't look right at this point, review all the steps and ask for help.  You'll want everything working before you proceed to the next step.**

- Make a minor change to your webserver:

  (use vim or vi if experienced enough, or just use the Google Cloud Shell built-in editor):

  ```bash
  cd ~/sfs/mywebserver
  ```
open the file **customize.yml**

    change the "name:" value from this:

    ```yaml
    name: 'Software Freedom School OWASP Juice Box'
    ```

    to something else, put whatever you want:

    ```yaml
    name: 'DevSecOps Rocks!'
    ```

    save file, return to the bash terminal, and push your changes up to your remote repo

  ```bash
  git add customize.yml
  git commit -m 'second webpage'
  git push
  ```

- Now go back to the Gitlab Job status page and check again to see that your change is building and testing again in your CI pipeline!

---

### Other Tools/Resources

- [GitLab](https://docs.gitlab.com/ce/ci/yaml/README.html)  
- [CircleCI](https://circleci.com/docs/getting-started/)  
- [Docker-CI](http://docker-ci.org/documentation)  
- [Jenkins](https://jenkins.io/doc/)  
- [Travis CI](https://docs.travis-ci.com/user/getting-started/)  

---

_Continuous Integration_ is a software development practice where members of a team integrate their work frequently, usually each person integrates at least daily - leading to multiple integrations per day. Each integration is verified by an automated build (including test) to detect integration errors as quickly as possible. Many teams find that this approach leads to significantly reduced integration problems and allows a team to develop cohesive software more rapidly.

-- Martin Fowler

---

|Previous: [Set up and containerized WAF](/labs/00_containerized_waf/README.md)|
|:---:|
